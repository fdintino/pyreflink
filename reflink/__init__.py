# -*- coding: utf-8 -*-

"""Top-level package for Python reflink."""

__author__ = """Ruben De Smet"""
__email__ = 'pypi@rubdos.be'
__version__ = '0.1.3'

from .reflink import reflink, supported_at
from .error import ReflinkImpossibleError
