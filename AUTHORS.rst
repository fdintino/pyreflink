=======
Credits
=======

Development Lead
----------------

* Ruben De Smet <pypi@rubdos.be>

Contributors
------------

None yet. Why not be the first?
